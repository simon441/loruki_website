# vuz-math-quiz

Live Coding a Math Quiz Web App with Vue
by Web Dev Junkie

video link: [https://youtu.be/y7dh6NrwlPI]

## Project setup
```
yarn install
```

### Compiles and hot-reloads for development
```
yarn serve
```

### Compiles and minifies for production
```
yarn build
```

### Lints and fixes files
```
yarn lint
```

### Customize configuration
See [Configuration Reference](https://cli.vuejs.org/config/).
